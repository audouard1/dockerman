package fr.isima.dockerman.model

data class LoginInfo (
    val username: String,
    val password: String
)