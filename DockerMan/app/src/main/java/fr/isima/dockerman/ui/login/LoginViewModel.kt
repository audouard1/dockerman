package fr.isima.dockerman.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import fr.isima.dockerman.data.repository.PortainerRepository
import fr.isima.dockerman.model.LoginInfo

class LoginViewModel : ViewModel(){

    private val repository : PortainerRepository = PortainerRepository()

    fun changeUrl(url : String){
        repository.changeUrl(url)
    }

    fun login(username : String, password : String) : LiveData<String> {
        return repository.postLogin(LoginInfo(username, password))
    }
}