package fr.isima.dockerman.service

import fr.isima.dockerman.model.LoginInfo
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

data class LoginResult(var jwt : String)

interface WebPortainerService {
    @POST("auth")
    fun login(@Body user: LoginInfo): Call<LoginResult>

    /*@GET("endpoints/1/docker/containers/json?all=1")
    fun getinfo(@Header("Authorization") authHeader : String ): Call<DockerState>*/
}