package fr.isima.dockerman.data.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import fr.isima.dockerman.model.LoginInfo
import fr.isima.dockerman.service.LoginResult
import fr.isima.dockerman.service.WebPortainerService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



class PortainerRepository {

    private lateinit var retrofit: Retrofit

    lateinit var service : WebPortainerService

    fun changeUrl(url : String){
        retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service  = retrofit.create(WebPortainerService::class.java)
    }

    @WorkerThread

    fun postLogin(loginInfo: LoginInfo) : LiveData<String> {
        val data = MutableLiveData<String>()
        service.login(loginInfo).enqueue(object : Callback<LoginResult> {
            override fun onFailure(call: Call<LoginResult>, t: Throwable) {
                TODO("not implemented")
            }

            override fun onResponse(call: Call<LoginResult>, response: Response<LoginResult>) {
                if(response.body() != null){
                    data.value = response.body()!!.jwt
                }else{
                    data.value = ""
                }
            }
        })
        return data
    }
    /*@WorkerThread

    fun getinfo(token : String) : LiveData<DockerState> {
        val data = MutableLiveData<DockerState>()
        service.getinfo(token).enqueue(object : Callback<DockerState> {
            override fun onFailure(call: Call<DockerState>, t: Throwable) {
                TODO("not implemented")
            }

            override fun onResponse(call: Call<DockerState>, response: Response<DockerState>) {
                data.value = response.body()
            }
        })
        return data
    }*/
}