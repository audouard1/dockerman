package fr.isima.dockerman.ui.login

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import fr.isima.dockerman.R

@Composable
fun LoginScreen(viewModel : LoginViewModel, context : Context) {
    var username by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var portainerUrl by remember { mutableStateOf("portainer.douard.me/api/") }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
    ) {
        LoginFields(
            username,
            password,
            portainerUrl,
            onLoginClick = {
                viewModel.changeUrl("https://$portainerUrl")
                viewModel.login(username, password) },
            onUsernameChange = { username = it },
            onPasswordChange = { password = it },
            onPortainerUrlChange = { portainerUrl = it },
            context = context
        )
    }
}

@Composable
fun LoginFields(
    username: String,
    password: String,
    portainerUrl: String,
    onUsernameChange: (String) -> Unit,
    onPasswordChange: (String) -> Unit,
    onPortainerUrlChange: (String) -> Unit,
    onLoginClick: (String) -> Unit,
    context: Context
) {
    val focusManager = LocalFocusManager.current

    Column(
        modifier = Modifier
            .fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(25.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(stringResource(R.string.Pleaselogin))

        OutlinedTextField(
            value = portainerUrl,
            placeholder = { Text(text = stringResource(R.string.portainerApiPlaceholder)) },
            label = { Text(text = stringResource(R.string.portainerAPI)) },
            onValueChange = onPortainerUrlChange,
            keyboardOptions = KeyboardOptions(imeAction = androidx.compose.ui.text.input.ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
        )

        OutlinedTextField(
            value = username,
            placeholder = { Text(text = stringResource(R.string.userPlaceholder)) },
            label = { Text(text = stringResource(R.string.username)) },
            onValueChange = onUsernameChange,
            keyboardOptions = KeyboardOptions(imeAction = androidx.compose.ui.text.input.ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
        )

        OutlinedTextField(
            value = password,
            placeholder = { Text(text = stringResource(R.string.password)) },
            label = { Text(text = stringResource(R.string.password)) },
            onValueChange = onPasswordChange,
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(imeAction = androidx.compose.ui.text.input.ImeAction.Done,
                keyboardType = KeyboardType.Password),
            keyboardActions = KeyboardActions (onDone = {
                focusManager.clearFocus()
                onLoginClick(username)
            })
        )

        Button(onClick = {
            if (username.isNotBlank() && password.isNotBlank()) {
                onLoginClick(username)
            } else {
                Toast.makeText(
                    context,
                    context.getText(R.string.invalidLoginArgs),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }) {
            Text(stringResource(R.string.login))
        }
    }
}
