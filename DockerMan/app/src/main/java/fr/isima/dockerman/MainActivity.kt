package fr.isima.dockerman

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import fr.isima.dockerman.ui.login.LoginScreen
import fr.isima.dockerman.ui.login.LoginViewModel
import fr.isima.dockerman.ui.theme.DockerManTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DockerManTheme {
                // A surface container using the 'background' color from the theme
                val viewModel = LoginViewModel()
                LoginScreen(viewModel = viewModel, applicationContext)
                /*Surface(color = MaterialTheme.colors.background) {
                    Greeting("Android")
                }*/
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DockerManTheme {
        Greeting("Android")
    }
}